﻿using UnityEngine;
using System.Collections;

public class BackgroundParellax : MonoBehaviour {

	public GameObject Player;
	public float parallaxFactor;

	private Vector3 PlayerStart;
	private Vector3 ParallaxStart;

	// Use this for initialization
	void Start () {
		PlayerStart = Player.transform.position;
		ParallaxStart = transform.position;
	}
	
	// Update is called once per frame
	void Update (){
		transform.position = ParallaxStart - parallaxFactor * (Player.transform.position - PlayerStart);
	}
}
