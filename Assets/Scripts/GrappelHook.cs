using UnityEngine;
using System.Collections;

public class GrappelHook : MonoBehaviour {
	public float maxDistance = 13.0f;
	public GameObject [] rivets;
	public float speed = .4f;
	public bool active = false;
	private bool found = false;
	private Rigidbody2D rb;
	private int nextHook = 0;
	private int hook;

	public AudioSource grappleAudio;

	LineRenderer grappleLine;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		grappleLine = GetComponent<LineRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(active && found){
			if(Input.GetKey(KeyCode.D)){
				hook = nextHook;
				GameObject next = rivets[hook];
				Vector3 temptest = next.transform.position;
				//temptest.y += 1;
				Vector3 newPosition = Vector3.MoveTowards (transform.position, temptest, speed);
				rb.MovePosition(newPosition);

				grappleLine.SetPosition(0,transform.position);
				grappleLine.SetPosition(1,temptest);
				grappleLine.enabled = true;
				grappleAudio.Play();
			}
			else
				grappleLine.enabled = false;
		}
		else
			grappleLine.enabled = false;
	}

	public void foundGrapple(){
		found = true;
	}

	public int getNextHook(){
		return nextHook;
	}

	public void setNextHook(int newVal){
		nextHook = newVal;
	}

	void fireGrapple(int location){
		RaycastHit hit;

		GameObject next = rivets[location];



		if (Physics.Raycast(transform.position, next.transform.position, out hit, maxDistance)) {
			if(hit.collider.gameObject.tag != "Rivet"){
				return;
			}else{
				//rb.gravityScale = 0;
				//transform.position = Vector3.MoveTowards(transform.position, next.transform.position, speed);
				Vector3 temptest = next.transform.position;
				temptest.y += 1;
				//Vector3 newPosition = Vector3.MoveTowards (transform.position, temptest, speed);
				rb.gameObject.transform.Translate(temptest * Time.deltaTime);
				//rb.MovePosition(temptest + transform.forward * Time.deltaTime);
				//rb.MovePosition(temptest);
				//i = (i+1)% arrayLength;
			}
		}
	}
}
