﻿using UnityEngine;
using System.Collections;

public class TrashController : MonoBehaviour {

	float intervalTimer;
	public float interval;
	public Transform trash;
	Vector3 startPosition;
	// Use this for initialization
	void Start () {
		intervalTimer = Time.time;
		startPosition = transform.position;
		Instantiate(trash, startPosition, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		//create trash object at set intervals
		if (Time.time >= intervalTimer + interval) {

			//GameObject trash = Instantiate(trash, new Vector3(0,0,0), Quaternion.identity) as GameObject;
			//trash.transform.parent = GameObject.FindGameObjectWithTag("trashConveyor").transform;

			Instantiate(trash, startPosition, Quaternion.identity);

			intervalTimer = Time.time;
		}
	}
}
