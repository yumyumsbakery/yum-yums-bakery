﻿using UnityEngine;
using System.Collections;

public class EnemyRangedController : MonoBehaviour {
	
	public GameObject target;
	public float chaseDistance = 7.0f;
	public float attackDistance = 5.0f;
	
	private AIComponents actions;

	// animator.setInteger("Action", x);
	// Action 0 = Idle
	// Action 1 = Moving
	// Action 2 = Attacking
	private Animator animator;
	
	// Use this for initialization
	void Start () {
		if(target == null) 
			target = GameObject.FindGameObjectWithTag ("Player");
		actions = gameObject.GetComponent<AIComponents>();
		animator = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		float tempDistance = Vector3.Distance (transform.position, target.transform.position);
		if (tempDistance > chaseDistance || actions.lineOfSight (gameObject, target, chaseDistance) == false) {
			actions.setCurrentState (AIComponents.ActionType.Patrolling);

			if(actions.isIdle()) 
				animator.SetInteger("Action", 0);
			else 
				animator.SetInteger("Action", 1);
		} else if (actions.lineOfSight (gameObject, target, chaseDistance) == true) {
			if (tempDistance > attackDistance) {
				animator.SetInteger("Action", 1);
				actions.setCurrentState (AIComponents.ActionType.Chasing);
			} else {
				animator.SetInteger("Action", 2);
				actions.setCurrentState(AIComponents.ActionType.RangedAttacking);
			}
		}
	}
}
