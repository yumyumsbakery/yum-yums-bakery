﻿using UnityEngine;
using System.Collections;

public class ExplosionController : MonoBehaviour {

	public int Damage;
	public float ExplosionTime;
	float stopTime;

	// Use this for initialization
	void Start () {
		stopTime = Time.time + ExplosionTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > stopTime)
			Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D co) {
		Debug.Log ("Explosion Collision");
		if (co.tag == "Enemy") {
			HasHealth health = co.gameObject.GetComponent<HasHealth> ();
			health.takeDamage (Damage);
		}
	}
}
