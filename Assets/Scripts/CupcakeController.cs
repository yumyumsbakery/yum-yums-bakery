﻿using UnityEngine;
using System.Collections;

public class CupcakeController : MonoBehaviour {

	public float vertSpeed;
	public float horzSpeed;
	public GameObject Explosion;
	private Vector2 startPos;
	private Vector2 velocity = Vector2.zero;

	// Use this for initialization
	void Start () {
		velocity.y = vertSpeed;

		GameObject weapon = GameObject.FindGameObjectWithTag("Weapon");

		//if getDir returns 0 player is facing right
		if (weapon.GetComponent<WeaponController> ().getDir () == 0)
			velocity.x = horzSpeed;
		else//facing left
			velocity.x = -horzSpeed;

		//set in motion
		GetComponent<Rigidbody2D> ().velocity = velocity;
	}

	void Update()
	{
		transform.Rotate (Vector3.forward * -10);
	}

	void OnTriggerEnter2D(Collider2D co) {
		if (co.tag == "Player" || co.tag == "SwimClimb")//ignore collision with player/pool/ladder
			return;

		Instantiate(Explosion, transform.position, transform.rotation);
				
		Destroy(gameObject);
		
	}
}
