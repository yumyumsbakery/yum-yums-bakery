﻿using UnityEngine;
using System.Collections;

public class Incinerator : MonoBehaviour {

	AudioSource fireAudio;

	void Start(){
		fireAudio = GetComponent<AudioSource>();
	}

	void OnTriggerEnter2D(Collider2D co) {

		fireAudio.Play ();

		//if game object collides with the incinerator destroy it
		if (co.tag == "Player") {
			HasHealth health = co.gameObject.GetComponent<HasHealth> ();
			health.playerReset();
		} else {
			Destroy (co.gameObject);
		}
		
	}
}
