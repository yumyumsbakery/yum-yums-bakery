﻿using UnityEngine;
using System.Collections;

public class CombinationLock : MonoBehaviour {

	public string nextButton;
	public string[] passWord;
	public int currButton;
	public bool codeGood;
	public bool codeStarted;
	public bool codeCorrect;
	public float stayRed;
	//wait int
	public int wait;


	// Use this for initialization
	void Start () {
		//init password array
		passWord = new string[5];
		// correct code is: circle, cross, pentagon, triangle
		passWord[0] = "circle";
		passWord[1] = "cross";
		passWord[2] = "pentagon";
		passWord[3] = "triangle";

		currButton = 0;

		nextButton = passWord[currButton];
		//code begins as good
		codeGood = true;
		//init code has not started
		codeStarted = false;
		//init codeCorrect
		codeCorrect = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}
