﻿using UnityEngine;
using System.Collections;

public class WinScreenScript : MonoBehaviour {

	public Rect menuRect;

	void OnGUI (){
		GUI.Window (0, menuRect, MainMenuFunction, "You Win!");
	}
	
	void MainMenuFunction(int arg)
	{
		if(GUILayout.Button ("Play Again?"))
			Application.LoadLevel ("Yum Yum's Bakery");
		if (GUILayout.Button ("Main Menu"))
			Application.LoadLevel ("Main Menu");
		if(GUILayout.Button ("Quit"))
			Application.Quit();
	}

}
