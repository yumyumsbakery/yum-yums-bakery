﻿using UnityEngine;
using System.Collections;

public class ButtonState : MonoBehaviour {

	public string buttonType;

	//triangle
	public Sprite tZeroState;
	public Sprite tWrongState;
	public Sprite tCorrectState;

	//circle
	public Sprite cZeroState;
	public Sprite cWrongState;
	public Sprite cCorrectState;
	//pentagon
	public Sprite pZeroState;
	public Sprite pWrongState;
	public Sprite pCorrectState;
	//cross
	public Sprite sZeroState;
	public Sprite sWrongState;
	public Sprite sCorrectState;
	//current sprites
	Sprite currentZeroSprite;
	Sprite currentCorSprite;
	Sprite currentICorSprite;
	//current index
	int currentIndex;
	//reset flag
	bool resetFlag;
	//access parent
	CombinationLock parent;
	//SpriteRenderer
	SpriteRenderer sr;

	public AudioSource buttonAudio;

//	private float stayRed;

	int state;
	// Use this for initialization
	void Start () {
		//init SriteRender 
		this.sr = GetComponent<SpriteRenderer> ();
		//init parent
		parent = transform.parent.GetComponent<CombinationLock> () as CombinationLock;
		//init wait
//		this.wait = 0;
		//init reset flag
		this.resetFlag = false;
		//init with zero state
		switch (buttonType) {
		case "triangle":
			this.currentZeroSprite = tZeroState;
			this.sr.sprite = currentZeroSprite;
			this.currentCorSprite = tCorrectState;
			this.currentICorSprite = tWrongState;
			this.currentIndex = 3;
			break;
		case "circle":
			this.currentZeroSprite = cZeroState;
			this.sr.sprite = currentZeroSprite;
			this.currentCorSprite = cCorrectState;
			this.currentICorSprite = cWrongState;
			this.currentIndex = 0;
			break;
		case "pentagon":
			this.currentZeroSprite = pZeroState;
			this.sr.sprite = currentZeroSprite;
			this.currentCorSprite = pCorrectState;
			this.currentICorSprite = pWrongState;
			this.currentIndex = 2;
			break;
		case "cross":
			this.currentZeroSprite = sZeroState;
			this.sr.sprite = currentZeroSprite;
			this.currentCorSprite = sCorrectState;
			this.currentICorSprite = sWrongState;
			this.currentIndex = 1;
			break;
		default:
			sr.sprite = tZeroState;
			break;
		}

	}
	
	// Update is called once per frame
	/*
	 * if code is good and code has started: 			button should be green if button index is 
	 * 													it's own index or greater, else button should
	 * 													be grey
	 * if code is good and code has not started:		button should be grey
	 * if code is not good and code has started:		button should be red if Time.time is < stayRed
	 * 													if Time.time > stayRed button should be grey
	 * 													reset codeStarted
	 * if code is not good and code has not started:	button should be grey
	 * 
	 */
	void Update () {

		//codeNotGood & codeNotStarted (0,0) = grey
		if (!parent.codeGood && !parent.codeStarted) {

			//set button to zero state
			this.sr.sprite = currentZeroSprite;
		}
		//codeNotGood & codeStarted (0,1) = red
		//if Time.time < stayRed = red
		//else Time.time > stayRed = grey, reset codeStarted
		if (!parent.codeGood && parent.codeStarted) {

			//set button to incorrect
			this.sr.sprite = this.currentICorSprite;
//			Debug.Log ("Start");
//			Debug.Log (Time.time);
//			Debug.Log (parent.stayRed);
		}
		if(!parent.codeGood && parent.codeStarted && Time.time > parent.stayRed){

			//set button to zero state
			this.sr.sprite = currentZeroSprite;
			//reset codeStarted
			parent.codeStarted = false;
			//reset codeGood
			parent.codeGood = true;
//			Debug.Log ("done");
//			Debug.Log (Time.time);
		}
		//codeGood & codeNotStarted (1,0) = grey
		if(parent.codeGood && !parent.codeStarted){

			//set button to zero state
			this.sr.sprite = currentZeroSprite;

		}
		//codeGood & codeStarted (1,1)
		//if button index is current index = green
		//else = grey
		if (parent.codeGood && parent.codeStarted && parent.currButton > currentIndex) {

			//set button to correct state
			this.sr.sprite = currentCorSprite; 

		} else if(parent.codeGood && parent.codeStarted && parent.currButton < currentIndex){

			//set button to zero state
			this.sr.sprite = currentZeroSprite;

		}

	}

	void OnTriggerEnter2D(Collider2D co) {
		if (parent.currButton > 0) {
			//make codeStarted true
			parent.codeStarted = true; //Depricate?
		}
		//change state of button
		if (co.tag == "Player") {

			buttonAudio.Play ();

			if (parent.codeGood && parent.nextButton == buttonType) {
				//increment current password index
				parent.currButton++;
				//update next button
				parent.nextButton = parent.passWord [parent.currButton];
				//update sprite
				this.sr.sprite = currentCorSprite;
				//start code
				parent.codeStarted = true;
			} else {
				//update sprite
				this.sr.sprite = currentICorSprite;
				//reset current password index
				parent.currButton = 0;
				//set stay red
				parent.stayRed = Time.time + parent.wait;
//				Debug.Log (parent.stayRed);
				//reset next button
				parent.nextButton = parent.passWord [parent.currButton];
				//set codeGood to false
				parent.codeGood = false;
				//set codeStarted to false
				parent.codeStarted = true;
			}
		} else {
			//do nothing
		}
		if (parent.codeGood && parent.currButton == 4) {
			//set codeCorrect
			parent.codeCorrect = true;
		}
	}
}
