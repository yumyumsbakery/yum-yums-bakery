﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {

	public float speed;
	public float distance;
	public int damage;
	private Vector2 startPos;
	private Vector2 stopPos1;
	private Vector2 stopPos2;
	private Vector2 velocity = Vector2.zero;

	// Use this for initialization
	void Start () {
		GameObject weapon = GameObject.FindGameObjectWithTag("Weapon");

		//track where each shot originated from
		startPos = transform.position;

		//set the distance each shot may travel
		stopPos1 = startPos;
		stopPos1.x += distance;
		stopPos2 = startPos;
		stopPos2.x -= distance;



		//if getDir returns 0 player is facing right
		if (weapon.GetComponent<WeaponController>().getDir() == 0){
			velocity.x = speed;
		}
		//if getDir returns 1 player is facing left
		if (weapon.GetComponent<WeaponController>().getDir() == 1){
			velocity.x = -speed;
		}

		//make the shot move in the direction specified by getDir()
		GetComponent<Rigidbody2D> ().velocity = velocity;
	}

	void FixedUpdate(){
		//Destroy the shot after its travelled a certain distance
		if(transform.position.x >= stopPos1.x || transform.position.x <= stopPos2.x){
			Destroy(gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D co) { 
		//if shot collides with an enemy reduce the enemy's health
		if (co.gameObject.tag == "Enemy") {
			HasHealth health = co.gameObject.GetComponent<HasHealth> ();
			health.takeDamage (damage);
		}if (co.gameObject.tag == "Trash"){
			Destroy(co.gameObject);
		}
		Destroy(gameObject);

	}

	void OnTriggerEnter2D(Collider2D co){
		//if shot collides with an enemy reduce the enemy's health
		if (co.tag == "Enemy") {
			HasHealth health = co.gameObject.GetComponent<HasHealth> ();
			health.takeDamage (damage);
		}if (co.tag == "Trash"){
			Destroy(co.gameObject);
		}
		if(!co.CompareTag("Rivet") && !co.CompareTag("GrappleControlUnit") && !co.CompareTag("Pick Up")){
			Destroy(gameObject);
		}
	}
}
