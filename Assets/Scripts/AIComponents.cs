﻿using UnityEngine;
using System.Collections;

public class AIComponents : MonoBehaviour {

	// All possible states that an enemy can be in
	public enum ActionType {
		Idle,
		MovingForward,
		Flip,
		Patrolling,
		Chasing,
		MeleeAttacking,
		RangedAttacking,
		Bobbing,
		Charging
	}
	public float moveSpeed = 3.0f;
	public float chaseSpeed = 4.0f;
	public float chargeSpeed = 10.0f;
	public float patrolDistance = 6.0f;
	public float minPatrolWaitTime = 1.0f;
	public float maxPatrolWaitTime = 2.5f;
	public int meleeDamage = 20;
	public float attackInterval = 1.5f;
	public GameObject target;
	public GameObject projectilePrefab;
	public Vector3 projectileSpawnOffset;

	public AudioSource shootSound;

	private ActionType currentState = ActionType.Idle;
	private Vector3 forwardDirection, chargePosition;
	private float randomWait = -1.0f;
	private float patrolWaitTimer = 0.0f;
	private float startPosition = 0.0f;
	private float nextAttack = 0.0f;
	private float nextCollisionDamage = 0.0f;
	private bool currentlyCharging = false;
	private bool enemyIsIdle = false;

	// animator.setInteger("Action", x);
	// Action 0 = Idle
	// Action 1 = Moving
	// Action 2 = Attacking
	private Animator animator;

	// Use this for initialization
	void Start () {
		forwardDirection = transform.right;
		startPosition = transform.position.x;
		chargePosition = Vector3.zero;
		if(target == null) 
			target = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		switch (currentState) {
		case ActionType.Idle:
			handleIdleState();
			break;
		case ActionType.MovingForward:
			handleMovingForwardState(moveSpeed);
			break;
		case ActionType.Flip:
			handleFlipState();
			break;
		case ActionType.Patrolling:
			handlePatrollingState();
			break;
		case ActionType.Chasing:
			handleChasingState();
			break;
		case ActionType.MeleeAttacking:
			handleMeleeAttackingState();
			break;
		case ActionType.RangedAttacking:
			handleRangedAttackingState();
			break;
		case ActionType.Bobbing:
			handleBobbingState();
			break;
		case ActionType.Charging:
			handleChargingState();
			break;
		}
	}

	void handleIdleState() {
		if (transform.GetComponent<Rigidbody2D>().velocity.y >= 0)
			transform.GetComponent<Rigidbody2D> ().velocity = new Vector2(0.0f, 0.0f);
	}

	void handleMovingForwardState(float speedToMove) {
		if (transform.GetComponent<Rigidbody2D>().velocity.y >= 0)
			transform.GetComponent<Rigidbody2D> ().velocity = (forwardDirection * speedToMove);
	}

	void handleFlipState() {
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		forwardDirection = -forwardDirection;
	}

	void handlePatrollingState() {
		if (transform.position.x > (startPosition + patrolDistance) || transform.position.x < (startPosition - patrolDistance)) {
			if(randomWait == -1.0f) 
				randomWait = Random.Range(minPatrolWaitTime, maxPatrolWaitTime);

			// Add to the timer
			patrolWaitTimer += Time.deltaTime;

			// Turn the enemy around and start the patrol again
			if(patrolWaitTimer > randomWait) {
				handleFlipState();
				startPosition = transform.position.x;
				patrolWaitTimer = 0.0f;
				randomWait = -1.0f;
				enemyIsIdle = true;
			} else {
				handleIdleState();
				enemyIsIdle = true;
			}
		} else {
			handleMovingForwardState(moveSpeed);
			enemyIsIdle = false;
		}
	}

	void handleChasingState() {
		bool linedUp = false;
		if (Mathf.Abs(transform.position.x - target.transform.position.x) < 0.5f) {
			handleIdleState ();
			linedUp = true;
		} else if (isFacingTarget() == false) 
			handleFlipState ();
		if(linedUp == false) handleMovingForwardState(chaseSpeed);
	}

	void handleMeleeAttackingState() {
		if (Time.time > nextAttack) {
			nextAttack = Time.time + attackInterval;
			HasHealth health = target.GetComponent<HasHealth> ();
			health.takeDamage (meleeDamage);
		}
	}

	void handleRangedAttackingState() {
		if (projectilePrefab != null) {
			if(isFacingTarget() == false)
				handleFlipState();
			if (Time.time > nextAttack) {
				nextAttack = Time.time + attackInterval;
				// Spawn projectile
				float dir = (forwardDirection.x > 0) ? 1.0f : -1.0f;
				GameObject clone = Instantiate (projectilePrefab, new Vector3(transform.position.x + projectileSpawnOffset.x * dir, transform.position.y + projectileSpawnOffset.y, transform.position.z + projectileSpawnOffset.z), transform.rotation) as GameObject;
				Projectile projectileInfo = clone.GetComponent<Projectile>();
				clone.GetComponent<Rigidbody2D>().velocity = new Vector2(forwardDirection.x * projectileInfo.projectileSpeed, 0.0f);
				shootSound.Play();
			}
		}
	}

	void handleBobbingState() {
		Debug.Log ("Bobbing State");
	}

	void handleChargingState() {
		currentlyCharging = true;
		if (chargePosition == Vector3.zero) {
			chargePosition = target.transform.position;
		}
		transform.position = Vector3.MoveTowards (transform.position, new Vector3(chargePosition.x, transform.position.y, chargePosition.z), Time.deltaTime * chargeSpeed);
		if (Mathf.Abs(transform.position.x - chargePosition.x) < 0.5f) {
			chargePosition = Vector3.zero;
			currentlyCharging = false;
		}
	}

	void OnCollisionStay2D(Collision2D coll) {
		if (coll.gameObject == target) {
			if (Time.time > nextCollisionDamage) {
				nextCollisionDamage = Time.time + attackInterval;
				// Apply damage
				handleMeleeAttackingState();
			}
		}
	}

	public bool lineOfSight(GameObject current, GameObject target, float range) {
		RaycastHit2D hit = Physics2D.Raycast(current.transform.position, target.transform.position - current.transform.position, range);
		if (hit && hit.transform && hit.transform.tag == target.tag)
			return true;
		else
			return false;
	}

	public bool isCharging() {
		return currentlyCharging;
	}

	public bool isFacingTarget() {
		var relativePoint = transform.InverseTransformPoint(target.transform.position);
		if (relativePoint.x < 0.0) { // Target is to the left
			return false;
		}
		return true;
	}
	
	public void setCurrentState(ActionType newState) {
		currentState = newState;
	}

	public bool isIdle() {
		return enemyIsIdle;
	}
}
