﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {
	
	public float speed = 8f;
	public float vertSpeed = 8f;
	public float jump = 20f;
	float move;
	Rigidbody2D playerRigidbody;
	bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.4f;
	public LayerMask whatIsGround;
    public float timeToDestroy = 2.0f;
	int countup;

    public bool Cheat;

	public GameObject IngUI;
	Dictionary<string, GameObject> IngredientsUI;
	bool recipeUI;//is recipe picked up?

	bool cupcakesEnabled = false;
	bool cupcakesUpgraded = false;
	public GameObject cupcake;
	public GameObject goocake;
	public float cupcakeFireRate;
	float nextCupcake = 0;

	bool SwimClimb = false;

	public bool dead = false;
	public Rect DeathScreenRect;

	public Rect PopupRect;
	string popUpMessage;
	public float popUpTime;
	float popUpDoneTime;

	public AudioSource JumpAudio;
	public AudioSource loseAudio;
	public AudioSource pickupAudio;

	void Start() {
		countup = 0;
		playerRigidbody = GetComponent<Rigidbody2D>();
		IngredientsUI = new Dictionary<string, GameObject>();
		foreach(Transform t in IngUI.transform)
		{
			IngredientsUI.Add(t.name, t.gameObject);
		}
	}

    void Update() {
        // Jumping
        Jump();
		//set invulnerable if applicable
		HasHealth myHealth = gameObject.GetComponent<HasHealth> ();
		myHealth.invulnerable = Cheat;
    }
	
	void FixedUpdate () {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);

		// Left and Right Movement
		if (Input.GetKey(KeyCode.LeftArrow)) {//Left Movement
			move = -1;
			//update the animations direction to left facing
			GetComponent<Animator> ().SetFloat ("Dir", 2.0f);
		} else if (Input.GetKey(KeyCode.RightArrow)) {//Right Movement
			move = 1;
			//update the animations direction to right facing
			GetComponent<Animator> ().SetFloat ("Dir", -2.0f);
		}
        else
        {
            move = 0;
            GetComponent<Animator>().SetFloat("Dir", 0f);
            GetComponent<Animator>().SetBool("Stop", true);
        }
		playerRigidbody.velocity = new Vector2 (move * speed, playerRigidbody.velocity.y);

		// Vetical Movement
		float vertMove = Input.GetAxis ("Vertical");
		if(SwimClimb)
			playerRigidbody.velocity = new Vector2 (playerRigidbody.velocity.x, vertMove * vertSpeed);

		if (Input.GetKey (KeyCode.F) && (cupcakesEnabled || Cheat) && Time.time > nextCupcake) {
			Vector2 t = new Vector2(transform.position.x, transform.position.y + 1);
			nextCupcake = Time.time + cupcakeFireRate;
			if (cupcakesUpgraded)
				Instantiate(goocake, t, transform.rotation);
			else//normal cupcakes
				Instantiate(cupcake, t, transform.rotation);
		}
	}

	void Jump() {
		if ((Input.GetKeyDown (KeyCode.UpArrow))&& (grounded || Cheat)) {
			playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, jump);
			GetComponent<Animator> ().SetBool ("Stop", true);
			JumpAudio.Play();
		}
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.gameObject.CompareTag ("Pick Up"))
		{
			CollectItem(other.gameObject.name);
			other.gameObject.SetActive (false);
			pickupAudio.Play();
		}

        // Incinerator reset
		else if (other.gameObject.CompareTag ("Incinerator"))
		{
			ResetPlayerPosition();
			other.gameObject.SetActive (true);
		}

		else if(other.name == "GrappleGun"){
			gameObject.GetComponent<GrappelHook>().foundGrapple();
			Destroy(other.gameObject);
			DisplayMessage("The inscription reads \"Grapplenator4000\"");
			pickupAudio.Play();
		}

		//can swim/climb ladder
		else if (other.gameObject.CompareTag ("SwimClimb"))
			SwimClimb = true;

		else if (other.name == "HintTrigger") {
			DisplayMessage("Look at that! An important hint burned into the wall under here!");
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag ("SwimClimb"))
			SwimClimb = false;
	}

    void OnCollisionStay2D(Collision2D c)
    {
        if (c.gameObject.CompareTag("Trap Door"))
        {
			countup++;
			if (countup > 100) {
            	Destroy(c.gameObject, timeToDestroy);
				countup = 0;
			}
        }
    }

	bool loseAudioPlayed = false;
	void OnGUI()
	{
		if(popUpDoneTime > Time.time)
			GUI.Window (0, PopupRect, (int IDictionary) => {GUILayout.Label(popUpMessage);}, "");
		if (dead) {
			if(!loseAudioPlayed){
				loseAudio.Play ();
				loseAudioPlayed = true;
			}
			Time.timeScale = 0;
			GUI.Window (0, DeathScreenRect, (int IDictionary) => 
			{
				GUILayout.Label ("You have died. Try again?");
				if (GUILayout.Button ("Try Again"))
					Application.LoadLevel ("Yum Yum's Bakery");
				if (GUILayout.Button ("Main Menu"))
					Application.LoadLevel ("Main Menu");
			}, "");
		}
	}

	void CollectItem(string name)
	{
		if (name == "RECIPE") {
			IngUI.SetActive (true);
			DisplayMessage ("The recipe for my cupcakes! Maybe I can find the ingredients I need on this ship?");
		}
		else {
			IngredientsUI [name].SetActive (true);
			if(IngredientsUI.ContainsKey(name + " Shadow"))
				IngredientsUI[name + " Shadow"].SetActive(false);
		}
		if (IngredientsUI ["EGG"].activeSelf && IngredientsUI ["FLOUR"].activeSelf && IngredientsUI ["SUGAR"].activeSelf)
		{
			cupcakesEnabled = true;
			DisplayMessage("You got everything you need for your famous explosive cupcakes!");
		}
		if (name == "GOO")
		{
			cupcakesUpgraded = true;
			DisplayMessage("What's this? Maybe I could use it in my cupcakes...");
		}
	}

	void ResetPlayerPosition() {
		dead = true;
		loseAudio.Play ();
	}

	void DisplayMessage(string message)
	{
		popUpMessage = message;
		popUpDoneTime = Time.time + popUpTime;
	}
}
