﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public Rect menuRect;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI (){
		GUI.Window (0, menuRect, MainMenuFunction, "Main Menu");
	}

	void MainMenuFunction(int arg)
	{
		if(GUILayout.Button ("Play"))
			Application.LoadLevel ("Yum Yum's Bakery");
		if(GUILayout.Button ("Quit"))
			Application.Quit();
	}
}
