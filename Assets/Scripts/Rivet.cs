﻿using UnityEngine;
using System.Collections;

public class Rivet : MonoBehaviour {
	
	public int nextHook = 0;
	private GameObject GrappleHookObj;

	public void Start(){
		GrappleHookObj = GameObject.Find ("Player");
	}

	public void OnCollisionEnter2D(Collision2D co){
		if (gameObject.name == "GrappleControlUnit" || gameObject.name == "GrappleControlUnit") {
			GrappleHookObj.GetComponent<GrappelHook>().active = false;
		}else if (gameObject.tag == "Rivet") {
			GrappleHookObj.GetComponent<GrappelHook>().active = true;
		}

		if (co.gameObject.tag == "Player") {
			co.gameObject.GetComponent<GrappelHook> ().setNextHook(nextHook);
		}
	}

	public void OnTriggerEnter2D(Collider2D co){
		if (gameObject.tag == "GrappleControlUnit" || gameObject.tag == "GrappleControlUnit") {
			GrappleHookObj.GetComponent<GrappelHook>().active = false;
		}else if (gameObject.tag == "Rivet") {
			GrappleHookObj.GetComponent<GrappelHook>().active = true;
		}

		if (co.gameObject.tag == "Player") {
			co.gameObject.GetComponent<GrappelHook> ().setNextHook(nextHook);
		}
	}
}
