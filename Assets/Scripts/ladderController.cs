﻿using UnityEngine;
using System.Collections;

public class ladderController : MonoBehaviour {

	//access parent
	CombinationLock parent;
	//rigidbody
	Rigidbody2D rb;
	//speed
	public float speed;
	public int distance;
	int tempDistance;
	// Use this for initialization
	void Start () {
		//init parent
		parent = transform.parent.GetComponent<CombinationLock> () as CombinationLock;
	}
	
	// Update is called once per frame
	void Update () {

		if (parent.codeCorrect && tempDistance <= distance) {
			transform.Translate(0,-speed,0);
			tempDistance++;
		}
	}
}
