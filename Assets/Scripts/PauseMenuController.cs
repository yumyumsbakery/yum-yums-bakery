using UnityEngine;
using System.Collections;

public class PauseMenuController : MonoBehaviour {

	bool paused;
	public Rect pauseRect;
	public GameObject Player;

	// Use this for initialization
	void Start () {
		paused = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Cancel"))
			paused = !paused;
		if (paused)
			Time.timeScale = 0;
		else
			Time.timeScale = 1;
	}

	void OnGUI (){
		if (paused)
			GUI.Window (0, pauseRect, PauseMenu, "Pause Menu");
	}

	void PauseMenu(int arg)
	{
		if(GUILayout.Button ("Resume"))
		  	paused = false;
		Player.GetComponent<PlayerController> ().Cheat = GUILayout.Toggle (Player.GetComponent<PlayerController> ().Cheat, "Cheat Mode");
		if(GUILayout.Button ("Main Menu"))
			Application.LoadLevel ("Main Menu");
		if(GUILayout.Button ("Quit"))
			Application.Quit();

	}
}
