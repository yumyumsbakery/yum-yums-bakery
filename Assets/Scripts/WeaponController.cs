using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour {

	public GameObject shot;
	public Transform shotSpawnRight;	//spawn point of the shot
	public Transform shotSpawnLeft;		//spawn point of the shot
	public float fireRate;
	public AudioSource shootAudio;
	
	private float nextFire;  			//time that the next shot can be taken at
	private int dir;					//direction of weapon

	void Start(){
		dir = 0;
	}

	// Update is called once per frame
	void Update () {
		//Check if weapon can fire again
		if (Input.GetKey(KeyCode.Space) && Time.time > nextFire)
		{
			//set time to next fire
			nextFire = Time.time + fireRate;

			shootAudio.Play();

			GameObject.FindWithTag("Player").GetComponent<Animator>().SetBool("Fire", true);
			//make sure the shot goes in the direction the weapon is facing
			if(dir == 0){
				Instantiate(shot, shotSpawnRight.position, shotSpawnRight.rotation);
			}else if (dir == 1){
				Instantiate(shot, shotSpawnLeft.position, shotSpawnLeft.rotation);
			}
		}
		if(Input.GetButtonUp("Fire1")){
			GameObject.FindWithTag("Player").GetComponent<Animator>().SetBool("Fire", false);
		}
	}

	void FixedUpdate(){

		if (Input.GetKey(KeyCode.RightArrow))
        {
			//update the animations direction
			//GetComponent<Animator>().SetFloat("Dir", 1.0f);
			//update weapon direction
			dir = 0;
		}
		if (Input.GetKey(KeyCode.LeftArrow))
        {
			//update the animations direction
			//GetComponent<Animator>().SetFloat("Dir", -1.0f);
			//update weapon direction
			dir = 1;
		}

	}

	//retruns the direction the weapon is pointing
	public int getDir(){
		return dir;
	}
}
