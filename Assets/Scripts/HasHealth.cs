using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HasHealth : MonoBehaviour {

	// Use this for initialization
	public int Health;
	public int Shields = 0;
	public GameObject shieldObject;
	public AudioSource damageAudio;

	private float MaxHealth;
	private float MaxShields; // Possibly use later for shield regeneration
	private GameObject shieldInstance;

	public bool invulnerable{ get; set;}

	public void Start(){
		MaxHealth = Health;
		MaxShields = Shields;
		if (Shields > 0 && shieldObject != null) {
			shieldInstance = Instantiate (shieldObject, transform.position, transform.rotation) as GameObject;
			shieldInstance.transform.parent = gameObject.transform;
			shieldInstance.transform.localScale = gameObject.GetComponent<SpriteRenderer>().bounds.size / 2;
		}
	}

	public void takeDamage(int damage){
		if (invulnerable)
			return;
		if (Shields > 0) {
			if(Shields - damage >= 0)
				Shields -= damage;
			else {
				Health -= Mathf.Abs(Shields - damage);
				Shields = 0;
				if(shieldObject != null) shieldInstance.GetComponent<SpriteRenderer>().enabled = false;
				updateHealthBar (Health);
				//if no health remaining destroy the object
				if (Health <= 0) {
					if (gameObject.tag == "Player") {
						playerReset ();
					} else {
						Destroy (gameObject);
					}
				}
			}
		} else {
			//play damage audio
			damageAudio.Play();
			//reduce health
			Health -= damage;
			updateHealthBar (Health);
			//if no health remaining destroy the object
			if (Health <= 0) {
				if (gameObject.tag == "Player") {
					playerReset ();
				} else {
					Destroy (gameObject);
				}
			}
		}
	}
	
	public void playerReset(){
		/*
		Vector2 restart = new Vector2 (-0.13f, .12f);
		gameObject.transform.position = restart;
		Health = (int)MaxHealth;
		*/
		PlayerController PC = GetComponent<PlayerController>();
		PC.dead = true;
	}

	public void updateHealthBar(int Health){
		float remainingHealth = Health / MaxHealth;
		GameObject healthBar = GameObject.Find ("Player Health");
		GameObject healthTextGameObject = GameObject.Find ("Health Text");
		Image healthBarImage = healthBar.GetComponent<Image> ();
		Text healthBarText = healthTextGameObject.GetComponent<Text> ();
		//Debug.Log("Health: " + remainingHealth);
		if (healthBarImage != null && healthBarText != null && gameObject.tag == "Player") {
			Debug.Log(gameObject.name);
			healthBarImage.fillAmount = remainingHealth;
			healthBarText.text = Health.ToString();
			/*
			if (remainingHealth > 0) {
				//Debug.Log("Alive: " + remainingHealth);
				healthBar.fillAmount = remainingHealth;
			} else {
				//Debug.Log("Dead: " + remainingHealth);
				healthBar.fillAmount = 0;
			}*/
		}
	}
}
