﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {
		
	public GameObject target;
	public float projectileSpeed = 20.0f;				// The speed of the projectile
	public float projectileDistance = 6.0f;				// The max distance you want the projectile to travel
	public int projectileDamage = 10;					// How much damage you want the projectile to inflict

	private Vector2 startPosition, endPosition, endPositionNeg;

	// Use this for initialization
	void Start () {
		if(target == null) 
			target = GameObject.FindGameObjectWithTag ("Player");
		startPosition = transform.position;
		endPosition = startPosition;
		endPosition.x = startPosition.x + projectileDistance;
		endPositionNeg = startPosition;
		endPositionNeg.x = startPosition.x - projectileDistance;
	}
	
	void FixedUpdate(){
		if (transform.position.x > endPosition.x || transform.position.x < endPositionNeg.x) {
			Destroy(gameObject);
		}
	}
	
	void OnTriggerEnter2D(Collider2D co) {
		if (co.gameObject == target) {
			HasHealth health = co.gameObject.GetComponent<HasHealth> ();
			health.takeDamage (projectileDamage);
			Destroy(gameObject);
		}
	}
}
