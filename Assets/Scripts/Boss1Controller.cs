﻿using UnityEngine;
using System.Collections;

public class Boss1Controller : MonoBehaviour {

	public GameObject bossTrigger;
	public float timeBetweenCharges = 7.0f;
	public AudioSource winAudio;

	private AIComponents actions;
	private float nextCharge;
	private HasHealth shieldValue;

	// animator.setInteger("Action", x);
	// Action 0 = Shield Idle
	// Action 1 = Shield Moving
	// Action 2 = Shield Damage
	// Action 3 = Idle
	// Action 4 = Walk
	// Action 5 = Damage
	private Animator animator;

	// Use this for initialization
	void Start () {
		actions = gameObject.GetComponent<AIComponents>();
		shieldValue = gameObject.GetComponent<HasHealth> ();
		animator = gameObject.GetComponent<Animator> ();
		nextCharge = timeBetweenCharges;
		if (bossTrigger == null)
			bossTrigger = GameObject.FindGameObjectWithTag ("BossTrigger");
	}
	
	// Update is called once per frame
	void Update () {
		if (bossTrigger.GetComponent<BossTriggerScript> ().bossStart == true) {
			if (Time.time > nextCharge) {
				nextCharge = Time.time + timeBetweenCharges;
				if(shieldValue.Shields > 0)
					animator.SetInteger("Action", 1);
				else
					animator.SetInteger("Action", 4);

				actions.setCurrentState (AIComponents.ActionType.Charging);
			} else if (actions.isCharging () == false) {
				if(shieldValue.Shields > 0)
					animator.SetInteger("Action", 0);
				else
					animator.SetInteger("Action", 3);

				actions.setCurrentState (AIComponents.ActionType.RangedAttacking);
			}
		} else {
			if(shieldValue.Shields > 0)
				animator.SetInteger("Action", 0);
			else
				animator.SetInteger("Action", 3);

			actions.setCurrentState(AIComponents.ActionType.Idle);
			nextCharge = Time.time + timeBetweenCharges;
		}
	}

	void OnDestroy() {
		if (bossTrigger && bossTrigger.GetComponent<BossTriggerScript> ().bossStart == true) {
			Time.timeScale = 0;
			if(gameObject.GetComponent<HasHealth>().Health <= 0){
				Application.LoadLevel ("Win Screen");
				winAudio.Play ();
			}
		}
	}
}
