﻿using UnityEngine;
using System.Collections;

public class BossTriggerScript : MonoBehaviour {

	public GameObject target;
	public bool bossStart = false;

	// Use this for initialization
	void Start () {
		if (target == null)
			target = GameObject.FindGameObjectWithTag ("Player");
	}

	void OnTriggerEnter2D(Collider2D co) {
		if (co.gameObject == target) {
			bossStart = true;
		}
	}
}
